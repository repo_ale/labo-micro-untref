/* 
 * File:   main.c
 * Author: ri_an
 *
 * Created on 26 de marzo de 2022, 13:24
 */
// PIC12F675 Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSC oscillator: I/O function on GP4/OSC2/CLKOUT pin, I/O function on GP5/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-Up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF      // GP3/MCLR pin function select (GP3/MCLR pin function is digital I/O, MCLR internally tied to VDD)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config CP = OFF         // Code Protection bit (Program Memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#define _XTAL_FREQ 4000000
/**/
int can_change;
int blinking;

int custom_delay_ms() {
    int flag = 0;
    
    for (int i=0; i<10; i++) {
        if (GPIObits.GP0) {
            flag = 1;
            break;
        } else {
            __delay_ms(100);        
        }
    }    
    return flag;
}

void blink() {    
    GPIObits.GP1 = 1;
            
    if (custom_delay_ms()) {
        GPIObits.GP1 = 0;
        return;
    }
    
    GPIObits.GP1 = 0;
    
    if (custom_delay_ms())       
        return;
}

int main() {
    OSCCAL = 0xff;
       
    // interrupts conf
    GIE = 0; 
    // interrupts confs
    
    CMCONbits.CM = 0B111;
    TRISIObits.TRISIO0 = 1;
    TRISIObits.TRISIO1 = 0;
    ANSELbits.ANS = 0b0000;
    GPIObits.GP0 = 0;
    GPIObits.GP1 = 0; // output
    can_change = 1;
    blinking = 0;
    
    while (1) {      
        if (can_change && GPIObits.GP0) {
            blinking = ~blinking;
            can_change = 0;
        }
        
        if (!GPIObits.GP0) {
            can_change = 1;
        }
        
        if(blinking) {
            blink();
        }
    }
}